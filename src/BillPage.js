import React from 'react';
import BillInfo from './components/BillInfo';
import Header from './components/Header';
import Footer from './components/Footer'

class BillPage extends React.Component{
  constructor(props){
    super(props);
    this.getBill = this.getBill.bind(this);
    this.setBill = this.setBill.bind(this);
    this.getBillStats = this.getBillStats.bind(this);
    this.setBillStats = this.setBillStats.bind(this);
    this.refreshUser = this.refreshUser.bind(this);
    this.state = {
      bill: null,
      percentApprove: null,
      userVote: null,
    }
  }

  componentDidMount(){
    this.getBill();
    this.getBillStats();
  };

  refreshUser(){
    this.getBillStats();
  }

  setBill(data){
    var response = JSON.parse(data.target.response);
    this.setState({
      bill: response.results[0],
    })
  };

  getBill(){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "https://congress.api.sunlightfoundation.com/bills?bill_id=" + this.props.params.bill_id, true);
    xmlHttp.addEventListener("load", this.setBill, false);
    xmlHttp.send(null);
  }

  setBillStats(data){
    console.log(data);
    var allVotes = JSON.parse(data.target.response);
    var pos = 0;
    var neg = 0;
    var user_vote = null;
    for(var i=0; i<allVotes.length; i++){
      if(allVotes[i].approve === "yes"){
        pos++;
      }else{
        neg++;
      }
      if(allVotes[i].user_id === parseInt(localStorage.user_id, 10)){
        user_vote = allVotes[i].approve
      }
    };
    var percent = (pos * 1.0) / (pos + neg);
    if(pos + neg === 0){
      percent = null;
    }
    this.setState({
      percentApprove: percent,
      userVote: user_vote
    });
  }

  getBillStats(data){
    console.log(data);
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", `http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/creative/getBillVotes.php?bill=${this.props.params.bill_id}`);
    xmlHttp.addEventListener("load", this.setBillStats, false);
    xmlHttp.send();
  }

  render(){
    return (
      <div>
        <Header refreshUser={this.refreshUser}/>
        <div className="main">
          {this.state.bill ? <BillInfo bill={this.state.bill} approvalRating={this.state.percentApprove} userVote={this.state.userVote} refreshVote={this.getBillStats}/> : null}
        </div>
        <Footer />
      </div>
    )
  }
}

export default BillPage;
