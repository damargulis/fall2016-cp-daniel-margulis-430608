import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import LegislatorPage from './LegislatorPage';
import BillPage from './BillPage';
import './index.css';

import { Router, Route, browserHistory } from 'react-router'

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={App} />
    <Route path="/legislators/:bioguide_id" component={LegislatorPage}/>
    <Route path="/bills/:bill_id" component={BillPage}/>
  </Router>),
  document.getElementById('root')
);
