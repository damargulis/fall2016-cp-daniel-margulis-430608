import React from 'react';
import './App.css';
import Header from './components/Header';
import LegislatorInfo from './components/LegislatorInfo';
import BillList from './components/BillList';
import PageControls from './components/PageControls';
import Footer from './components/Footer'

class LegislatorPage extends React.Component {
  constructor(props){
    super(props);
    this.getRepresentative = this.getRepresentative.bind(this);
    this.setRepresentative = this.setRepresentative.bind(this);
    this.getBills = this.getBills.bind(this);
    this.setBills = this.setBills.bind(this);
    this.gotoPage = this.gotoPage.bind(this);
    this.getActiveBills = this.getActiveBills.bind(this);
    this.getRepresentativeStats = this.getRepresentativeStats.bind(this);
    this.setRepresentativeStats = this.setRepresentativeStats.bind(this);
    this.refreshUser = this.refreshUser.bind(this);
    this.state = {
      representative: null,
      bills: [],
      page: 1,
      percentApprove: null,
      userVote: null,
    }
  }

  refreshUser(){
    this.getRepresentativeStats();
  }

  componentDidMount(){
    this.getRepresentative();
    this.getBills();
    this.getRepresentativeStats();
  }

  setRepresentative(data){
    var response = JSON.parse(data.target.response);
    this.setState({
      representative: response.results[0],
    })
  }

  getRepresentative(){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "https://congress.api.sunlightfoundation.com/legislators?bioguide_id=" + this.props.params.bioguide_id, true);
    xmlHttp.addEventListener("load", this.setRepresentative, false);
    xmlHttp.send(null);
  }

  setRepresentativeStats(data){
    var allVotes = JSON.parse(data.target.response);
    var pos = 0;
    var neg = 0;
    var user_vote = null;
    for(var i=0; i<allVotes.length; i++){
      if(allVotes[i].approve === "yes"){
        pos++;
      }else{
        neg++;
      }
      if(allVotes[i].user_id === parseInt(localStorage.user_id, 10)){
        user_vote = allVotes[i].approve
      }
    };
    var percent = (pos * 1.0) / (pos + neg);
    if(pos + neg === 0){
      percent = null;
    }
    this.setState({
      percentApprove: percent,
      userVote: user_vote
    });

  }

  getRepresentativeStats(data){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", `http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/creative/getLegislatorVotes.php?legislator=${this.props.params.bioguide_id}`);
    xmlHttp.addEventListener("load", this.setRepresentativeStats, false);
    xmlHttp.send();
  }

  setBills(data){
    var response = JSON.parse(data.target.response);
    this.setState({
      bills: response.results,
    })
  }

  getActiveBills(){
    let PER_PAGE = 10;
    var bills = this.state.bills.slice((this.state.page-1) * PER_PAGE, (this.state.page) * (PER_PAGE) );

    return bills
    }

  getBills(){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "https://congress.api.sunlightfoundation.com/bills?sponsor.bioguide_id=" + this.props.params.bioguide_id, true);
    xmlHttp.addEventListener("load", this.setBills, false);
    xmlHttp.send(null);
  }

  gotoPage(page){
    this.setState({
      page: page,
    })
  }

  render(){
    var bills = this.getActiveBills();
    var loggedIn = localStorage.user_id ? true : false;
    return (
      <div>
        <Header refreshUser={this.refreshUser}/>
        <div className="main">
          {this.state.representative ? <LegislatorInfo legislator={this.state.representative} approvalRating={this.state.percentApprove} userVote={this.state.userVote} refreshVote={this.getRepresentativeStats}/> : null}
          <div className="title">Sponsored Bills</div>
          <BillList bills={bills} />
          <PageControls page={this.state.page} gotoPage={this.gotoPage} />
        </div>
        <Footer />

      </div>
    )
  }
}

export default LegislatorPage;
