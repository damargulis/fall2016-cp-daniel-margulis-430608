import React from 'react';

class FeedbackBox extends React.Component{
  constructor(props){
    super(props);
    this.renderButtons = this.renderButtons.bind(this);
    this.onVote = this.onVote.bind(this);
  }

  onVote(vote){
    if(localStorage.user_id){
      this.props.onVote(vote);
    }else{
      alert("Login to vote");
    }
  }

  renderButtons(){
    if(this.props.userVote == null){
      return(
        <div>
          <button className="question green" onClick={() => this.onVote(true)}>Approve</button>
          <button className="question red" onClick={() => this.onVote(false)}>Disapprove</button>
        </div>
      )
    }else if (this.props.userVote === "yes") {
      return(
        <div>
          <button className="question green selected">Approve</button>
          <button className="question red" onClick={() => this.props.changeVote(false)}>Disapprove</button>
        </div>
      )
    }else if(this.props.userVote === "no"){
      return(
        <div>
          <button className="question green" onClick={() => this.props.changeVote(true)}>Approve</button>
          <button className="question red selected">Disapprove</button>
        </div>
      )
    }
  }

  render(){
    return (
      <div>
        {this.renderButtons()}
      </div>
    )
  }
};

export default FeedbackBox;
