import React from 'react';
import FeedbackBox from './FeedbackBox'

class LegislatorInfo extends React.Component{
  constructor(props){
    super(props);
    this.renderComponent = this.renderComponent.bind(this);
    this.onVote = this.onVote.bind(this);
    this.renderApproval = this.renderApproval.bind(this);
    this.renderUserApproval = this.renderUserApproval.bind(this);
    this.changeVote = this.changeVote.bind(this);
  }

  changeVote(approve){
    var data = `token=${localStorage.token}&legislator=${this.props.legislator.bioguide_id}&approve=${approve}&user_id=${localStorage.user_id}`;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/creative/changeLegislatorVote.php", true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.addEventListener("load", this.props.refreshVote, false);
    xmlHttp.send(data);
  }

  onVote(approve){
    var data = `token=${localStorage.token}&legislator=${this.props.legislator.bioguide_id}&approve=${approve}&user_id=${localStorage.user_id}`;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/creative/legislatorVote.php", true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.addEventListener("load", this.props.refreshVote, false);
    xmlHttp.send(data);
  }

  renderComponent(component,title){
    if(!component){
      return
    }else{
      return (
        <tr>
          <th>{title}</th>
          <td>{component}</td>
        </tr>
      )
    }
  }

  renderUserApproval(){
    if(this.props.userVote == null){
      return
    }else{
      return (
        <tr>
          <th>You Voted </th>
          <td>{this.props.userVote === "yes" ? "Approve" : "Disapprove"}</td>
        </tr>
      )
    }
  }

  renderApproval(){
    if(this.props.approvalRating == null){
      return;
    }else{
      return (
        <tr>
          <th>Approval Rating</th>
          <td>{this.props.approvalRating * 100}%</td>
        </tr>
      )
    }
  }

  render(){
    var {legislator} = this.props;
    var term;
    if(legislator.term_start && legislator.term_end){
      term = `${legislator.term_start} - ${legislator.term_end}`;
    }
    return (
      <div className="LegislatorInfo" >
        <table>
          <tbody>
            <tr>
              <th>{`${legislator.title}. ${legislator.first_name} ${legislator.last_name}`}</th>
            </tr>
            {this.renderComponent(legislator.chamber, 'Chamber')}
            {this.renderComponent(legislator.district, 'District')}
            {this.renderComponent(legislator.party, 'Party')}
            {this.renderComponent(legislator.state_name, 'State')}
            {this.renderComponent(term,'Term')}
            {this.renderApproval()}
            {this.renderUserApproval()}
          </tbody>
        </table>
        <FeedbackBox onVote={this.onVote} changeVote={this.changeVote} userVote={this.props.userVote}/>
      </div>
    )
  }
};

export default LegislatorInfo;
