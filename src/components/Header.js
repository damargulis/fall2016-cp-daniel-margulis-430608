import React from 'react';
import LogIn from './LogIn';
import UserInfo from './UserInfo';
import {Link} from 'react-router';


class Header extends React.Component {
  constructor(props){
    super(props);
    this.logout = this.logout.bind(this);
    this.setUser = this.setUser.bind(this);
    this.login = this.login.bind(this);
    this.register = this.register.bind(this);
    this.state = {
      username: localStorage.username,
      user_id: localStorage.user_id,
      token: localStorage.token,
    };
  }

  setUser(data){
    var response = JSON.parse(data.target.response);
    this.setState({
      username: response.username,
      user_id: response.user_id,
      token: response.token
    });
    localStorage.user_id = response.user_id;
    localStorage.username = response.username;
    localStorage.token = response.token;
    this.props.refreshUser();
  }

  logout(){
    localStorage.removeItem('username');
    localStorage.removeItem('user_id');
    localStorage.removeItem('token');
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/creative/logout.php", true);
    xmlHttp.send(null);
    this.setState({
      username: null,
      user_id: null,
      token: null
    });
    this.props.refreshUser();

  }

  login(user){
    var data = `username=${user.username}&password=${user.password}`;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/creative/login.php", true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.addEventListener("load", this.setUser, false);
    xmlHttp.send(data);
  }

  register(user){
    var data = `username=${user.username}&password=${user.password}`;

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/creative/register.php", true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.addEventListener("load", this.setUser, false);
    xmlHttp.send(data);
  }

  render() {
    return(
      <div className="header">
        <div className="title">
        Congress Tracker
        </div>
        <button className="menu"><Link to="/">Home</Link></button>
        {this.state.token ? <UserInfo username={this.state.username} logout={this.logout}/> : <LogIn setUser={this.login} register={this.register} />}
      </div>
    )
  }

}


export default Header;
