import React from 'react';

class PageControls extends React.Component{
  render(){
    return(
      <div>
        <button onClick={() => this.props.gotoPage(this.props.page - 1)}>Prev</button>
        <span>Page: {this.props.page} </span>
        <button onClick={() => this.props.gotoPage(this.props.page + 1)}>Next</button>
      </div>
    )
  }
};

export default PageControls;
