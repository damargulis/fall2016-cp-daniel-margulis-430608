import React, { Component } from 'react';

class LogIn extends Component {
  constructor(props){
    super(props);
    this.state = {
      username: '',
      password: '',
    }
    this.handleChangeUsername = this.handleChangeUsername.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.register = this.register.bind(this);
    this.login = this.login.bind(this);
  }

  handleChangeUsername(evt){
    this.setState({
      username: evt.target.value,
    })
  }

  handleChangePassword(evt){
    this.setState({
      password: evt.target.value,
    })
  }

  login(){
    this.props.setUser({username: this.state.username, password: this.state.password});
  }

  register(){
    this.props.register({
      username: this.state.username,
      password: this.state.password,
    })
  }

  render() {
    return (
      <div className="user-info">
        <label>Username: </label><input type="text" value={this.state.username} onChange={this.handleChangeUsername}></input><br></br>
        <label>Password: </label><input type="password" value={this.state.password} onChange={this.handleChangePassword}></input><br></br>
        <button onClick={this.login}>Login</button><button onClick={this.register}>Register</button>
      </div>
    )
  }
}
export default LogIn;
