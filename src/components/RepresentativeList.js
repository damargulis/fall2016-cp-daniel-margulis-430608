import React from 'react';
import {Link} from 'react-router';

class RepresentativeList extends React.Component {
  constructor(props){
    super(props);
    this.renderRepresentative = this.renderRepresentative.bind(this);
  }

  renderRepresentative(representative){
    return(
      <tr key={representative.bioguide_id}>
        <td><Link to={"/legislators/" + representative.bioguide_id}>{representative.first_name + " " + representative.last_name}</Link></td>
        <td>{representative.state_name}</td>
        <td>{representative.chamber.charAt(0).toUpperCase() + representative.chamber.slice(1)}</td>
        <td>{representative.party}</td>
      </tr>
    )
  }

  render(){

    return (
      <div>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>State</th>
              <th>Chamber</th>
              <th>Party</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.representatives.map(this.renderRepresentative)
            }
          </tbody>
        </table>
      </div>
    )
  }
}

export default RepresentativeList;
