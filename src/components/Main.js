import React from 'react';
import RepresentativeList from './RepresentativeList';
import PageControls from './PageControls';

class Main extends React.Component{
  constructor(props){
    super(props);
    this.getRepresentatives = this.getRepresentatives.bind(this);
    this.setRepresentatives = this.setRepresentatives.bind(this);
    this.gotoPage = this.gotoPage.bind(this);
    this.getActiveReps = this.getActiveReps.bind(this);
    this.handleSelectChamber = this.handleSelectChamber.bind(this);
    this.setSort = this.setSort.bind(this);
    this.state = {
      representatives: [],
      page: 1,
      chamber: 0,
      sort: 0,
    }
  };

  componentDidMount(){
    this.getRepresentatives();
  };

  setSort(sort){
    this.setState({
      sort: sort,
      page: 1
    });
  }

  handleSelectChamber(chamber){
    this.setState({
      chamber: chamber,
      page: 1
    })
  };

  setRepresentatives(data){
    var reps = JSON.parse(data.target.response);
    this.setState({
      representatives: reps.results.sort(function(a,b) {
        if(a.last_name < b.last_name){
          return -1;
        }else{
          return 1;
        }
      }),
    })
  };

  getRepresentatives(){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "https://congress.api.sunlightfoundation.com/legislators?per_page=all", true);
    xmlHttp.addEventListener("load", this.setRepresentatives, false);

    xmlHttp.send(null);
  };

  gotoPage(page){
    if(page < 1){
      return;
    }
    this.setState({
      page: page
    })
  }

  getActiveReps(){
    var reps = []
    var PER_PAGE = 20;
    if(this.state.chamber === 1){
      reps = this.state.representatives.filter((rep) => rep.chamber === "house");
    }else if(this.state.chamber === 2){
      reps = this.state.representatives.filter((rep) => rep.chamber === "senate")
    }else{
      reps = this.state.representatives
    }
    if(this.state.sort === 0){
      reps.sort(function(a,b){
        if(a.last_name < b.last_name){
          return -1;
        }else{
          return 1;
        }
      });
    }else if(this.state.sort === 1){
      reps.sort(function(a,b){
        if(a.state < b.state){
          return -1;
        }else{
          return 1;
        }
      })
    }
    reps = reps.slice((this.state.page-1) * PER_PAGE, (this.state.page) * (PER_PAGE) );

    return reps
  };

  render(){
    var menu_map = [
      'All Congresspeople',
      'All Representatives',
      'All Senators',
    ]
    var title = menu_map[this.state.chamber];
    var reps = this.getActiveReps();
    return(
      <div className="main">
        <div className="title">
          {title}
        </div>
        <div className="menu">
          <div>
            <button className="button" onClick={() => this.handleSelectChamber(0)}>All</button>
            <button className="button" onClick={() => this.handleSelectChamber(1)}>House</button>
            <button className="button" onClick={() => this.handleSelectChamber(2)}>Senate</button>
          </div>
          <div>
            Sort:
            <button className="button" onClick={() => this.setSort(0)}>By Name</button>
            <button className="button" onClick={() => this.setSort(1)}>By State</button>
          </div>
        </div>
        <RepresentativeList representatives={reps} />
        <PageControls gotoPage={this.gotoPage} page={this.state.page} />
      </div>
    )
  };
}

export default Main;
