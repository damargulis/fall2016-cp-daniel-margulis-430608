import React from 'react';
import FeedbackBox from './FeedbackBox';

class BillInfo extends React.Component{
  constructor(props){
    super(props);
    this.renderApproval = this.renderApproval.bind(this);
    this.renderUserApproval = this.renderUserApproval.bind(this);
    this.onVote = this.onVote.bind(this);
    this.changeVote = this.changeVote.bind(this);
  }

  changeVote(approve){
    var data = `token=${localStorage.token}&bill=${this.props.bill.bill_id}&approve=${approve}&user_id=${localStorage.user_id}`;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/creative/changeBillVote.php", true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.addEventListener("load", this.props.refreshVote, false);
    xmlHttp.send(data);
  }

  onVote(approve){
    var data = `token=${localStorage.token}&bill=${this.props.bill.bill_id}&approve=${approve}&user_id=${localStorage.user_id}`;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/creative/billVote.php", true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.addEventListener("load", this.props.refreshVote, false);
    xmlHttp.send(data);
  }

  renderUserApproval(){
    if(this.props.userVote == null){
      return
    }else{
      return (
        <tr>
          <th>You Voted </th>
          <td>{this.props.userVote === "yes" ? "Approve" : "Disapprove"}</td>
        </tr>
      )
    }
  }

  renderApproval(){
    if(this.props.approvalRating == null){
      return;
    }else{
      return (
        <tr>
          <th>Approval Rating</th>
          <td>{this.props.approvalRating * 100}%</td>
        </tr>
      )
    }
  }

  render(){
    var {bill} = this.props;
    return (
      <div className="BillInfo" >
        <table>
          <tbody>
            <tr>
              <th>Title</th>
              <td>{bill.official_title}</td>
            </tr>
            <tr>
              <th>Sponsor</th>
              <td>{`${bill.sponsor.first_name} ${bill.sponsor.last_name}`}</td>
            </tr>
            <tr>
              <th>Chamber</th>
              <td>{bill.chamber}</td>
            </tr>
            <tr>
              <th>Inrtoduced On</th>
              <td>{bill.introduced_on}</td>
            </tr>
            <tr>
              <th>Last Action</th>
              <td>{bill.last_action_at}</td>
            </tr>
            <tr>
              <th>View Bill</th>
              <td><a href={bill.urls.congress}>{bill.urls.congress}</a></td>
            </tr>
            {this.renderApproval()}
            {this.renderUserApproval()}
          </tbody>
        </table>
        <FeedbackBox onVote={this.onVote} changeVote={this.changeVote} userVote={this.props.userVote}/>

      </div>
    )
  }
}

export default BillInfo;
