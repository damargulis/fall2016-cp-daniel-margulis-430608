import React from 'react';
import {Link} from 'react-router';

class BillList extends React.Component{
  renderBill(bill){
    var title = bill.popular_title;

    title = title ? title : bill.short_title;
    title = title ? title : bill.official_title;

    var status;
    if(bill.history.active){
      status = "Active";
    }else if(bill.history.awaiting_signiture){
      status = "Awaiting Signiture";
    }else if(bill.history.enacted){
      status = "Enacted";
    }else if(bill.history.veoted){
      status = "Veoted";
    }
    return (
      <tr key={bill.bill_id}>
        <td><Link to={"/bills/" + bill.bill_id}>{title}</Link></td>
        <td>{bill.introduced_on}</td>
        <td>{status ? status : "None"}</td>
      </tr>
    )
  }

  render(){
    return(
      <table >
        <thead>
          <tr>
            <th>Title</th>
            <th>Introduction Date</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {
            this.props.bills.map(this.renderBill.bind(this))
          }
        </tbody>
      </table>
    )
  }
}

export default BillList;
