import React, { Component } from 'react';

class UserInfo extends Component {
  render() {
    return (
      <div className="user-info">
        <div>Welcome {this.props.username} <button onClick={() => this.props.logout(null)}>Log Out</button></div>
      </div>
    )
  }
}

export default UserInfo
