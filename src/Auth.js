
module.exports = {
  login(username, password, callback) {
    callback = arguments[arguments.length - 1]
    if (localStorage.token) {
      if (cb) cb(true)
      this.onChange(true)
      return
    }
    sendRequest(username, password, (res) => {
      if (res.authenticated) {
        localStorage.token = res.token
        if (cb) cb(true)
        this.onChange(true)
      } else {
        if (cb) cb(false)
        this.onChange(false)
      }
    })
  },

  getToken() {
    return localStorage.token
  },

  logout(cb) {
    delete localStorage.token
    if (cb) cb()
    this.onChange(false)
  },

  loggedIn() {
    return !!localStorage.token
  },

  onChange() {}
}

function pretendRequest(username, password, callback) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("POST", "/server/login.php");
  var data = new FormData();
  data.append("username", username);
  data.append("username", password);
  xmlHttp.addEventListener("load", callback, false);
  xmlHttp.send(data);
}
