<?php
	header('Access-Control-Allow-Origin: *');

	include 'accessDatabase.php';

	ini_set("session.cookie_httponly", 1);

	session_start();

	$previous_ua = @$_SESSION['useragent'];
	$current_ua = $_SERVER['HTTP_USER_AGENT'];
	
	if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
		die("SESSION hijack detected");
	}else{
		$_SESSION['useragent'] = $current_ua;
	}

	$leg_id = $_POST['legislator'];
	$approve = $_POST['approve'];

	if($approve == "true"){
		$vote = 'yes';
	}else{
		$vote = 'no';
	}
	$user_id = $_POST['user_id'];

	$stmt = $mysqli->prepare("insert into legislatorVotes (user_id, legislator_id, approve) values (?, ?, ?)");
	if(!$stmt){
		header("HTTP", true, 500);
		exit;
	}

	$stmt->bind_param('iss', $user_id, $leg_id, $vote);
	$stmt->execute();
	header("HTTP", true, 200);
	exit; 

	
?>
