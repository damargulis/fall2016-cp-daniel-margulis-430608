<?php

	header("Access-Control-Allow-Origin: *");

	include 'accessDatabase.php';

	ini_set("session.cookie_httponly", 1);
	session_start();
	session_name('congressTracker');

	$stmt = $mysqli->prepare("SELECT COUNT(*), id, password_hash FROM users WHERE username=(?)");
	if(!$stmt){
		header("HTTP", true, 401);
		exit;
	}

	$user = $_POST['username'];
	$stmt->bind_param('s', $user);
	$stmt->execute();

	$stmt->bind_result($cnt, $user_id, $pwd_hash);
	$stmt->fetch();

	$pwd_guess = $_POST['password'];
	if($cnt == 1 && crypt($pwd_guess, $pwd_hash) == $pwd_hash){
		$_SESSION['user_id'] = $user_id;
		$_SESSION['token'] = substr(md5(rand()), 0, 10);
		$_SESSION['username'] = $_POST['username'];
		$data = array(
			"user_id" => $user_id,
			"token" => $_SESSION['token'],
			"username" => $_POST['username']
		);
		header("Content-type: application/json");
		echo json_encode($data);
		exit;
	}else{
		header("HTTP", true, 401);
	}

	exit;	


?>
