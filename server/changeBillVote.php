<?php
	header("Access-Control-Allow-Origin: *");
	include 'accessDatabase.php';
	ini_set("session.cookiehttponly", 1);
	session_start();
	$previous_ua = @$_SESSION['useragent'];
	$current_ua = $_SERVER['HTTP_USER_AGENT'];
	if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
		die("SESSION hijack detected");
	}else{
		$_SESSION['useragent'] = $current_ua;
	}
	
	$user_id = $_POST['user_id'];
	$bill_id = $_POST['bill'];
	$approve = $POST['approve'];
	if($approve == 'true'){
		$vote = 'yes';
	}else{
		$vote = 'no';
	}
	
	$stmt = $mysqli->prepare('update billVotes set approve=(?) where user_id=(?) and bill_id=(?)');
	$stmt->bind_param('sis', $vote, $user_id, $bill_id);
	$stmt->execute();
	header("HTTP", true, 200);
	exit;

?>
