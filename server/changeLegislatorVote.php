<?php
	header("Access-Control-Allow-Origin: *");

	include 'accessDatabase.php';

	ini_set("session.cookie_httponly", 1);
	session_start();
	$previous_ua = @$_SESSION['useragent'];
	$current_ua = $_SERVER['HTTP_USER_AGENT'];

	if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
		die("Session hijack detected");
	}else{
		$_SESSION['useragent'] = $current_ua;
	}
	
	$user_id = $_POST['user_id'];
	$leg_id = $_POST['legislator'];
	$approve = $_POST['approve'];
	
	if($approve == 'true'){
		$vote = 'yes';
	}else{
		$vote = 'no';
	}

	$stmt = $mysqli->prepare('update legislatorVotes set approve=(?) where user_id=(?) and legislator_id=(?)');
	$stmt->bind_param('sis', $vote, $user_id, $leg_id);
	$stmt->execute();
	header("HTTP", true, 200);
	exit;	
?>
