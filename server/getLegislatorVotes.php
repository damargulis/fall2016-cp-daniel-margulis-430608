<?php
	header('Access-Control-Allow-Origin: *');

	include 'accessDatabase.php';
	
	ini_set("session.cookiehttponly", 1);
	session_start();

	$previous_ua = @$_SESSION['useragent'];
	$current_ua = $_SERVER['HTTP_USER_AGENT'];

	if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
		die("Session hijack detected");
	}else{
		$_SESSION['useragent'] = $current_ua;
	}
	
	$leg_id = $_GET['legislator'];

	$stmt = $mysqli->prepare("select * from legislatorVotes where legislator_id = (?)");

	$stmt->bind_param('s', $leg_id);
	$stmt->execute();

	$myArray = array();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()){
		$myArray[] = $row;
	}

	header('Content-type: application/json');
	echo json_encode($myArray);
	exit;

?>
