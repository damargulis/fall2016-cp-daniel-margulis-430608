<?php
  header('Access-Control-Allow-Origin: *');

  include 'accessDatabase.php';

  ini_set("session.cookie_httponly", 1);
  session_start();
  session_name('congressTracker');

  $username = $_POST['username'];
  $password = $_POST['password'];

  //check if username already exists
  $stmt = $mysqli->prepare("select * from users where username=(?) LIMIT 1");
  if(!$stmt){
    header("HTTP", true, 401);
    exit;
  }

  $stmt->bind_param('s', $username);
  $stmt->execute();
  $result = $stmt->get_result();
  if($result->fetch_assoc()){
    //user already exists
    header("HTTP", true, 500);
    exit;
  }
  $stmt->close();

  $stmt2 = $mysqli->prepare("insert into users (username, password_hash) values (?,?)");
  if(!$stmt2){
    header("HTTP", true, 401);
    exit;
  }

  $stmt2->bind_param('ss', $username, crypt($password));
  $stmt2->execute();

  $id = $stmt2->insert_id;
  $stmt2->close();

  $_SESSION['user_id'] = $id;
  $_SESSION['token'] = substr(md5(rand()), 0, 10);
  $_SESSION['username'] = $_POST['username'];
  header('Content-type: application/json');
  $data = array(
    "user_id" => $id,
    "token" => $_SESSION['token'],
    "username" => $_POST['username']
  );

  echo json_encode($data);

?>
