<?php
	header('Access-Control-Allow-Origin: *');
	include 'accessDatabase.php';
	ini_set("session.cookiehttponly", 1);
	session_start();
	
	$bill_id = $_GET['bill'];
	
	$stmt = $mysqli->prepare("select * from billVotes where bill_id = (?)");
	
	$stmt->bind_param('s', $bill_id);
	$stmt->execute();
	$myArray = array();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()){
		$myArray[] = $row;
	}

	header('Content-type: application/json');
	echo json_encode($myArray);
	exit;	

?>
