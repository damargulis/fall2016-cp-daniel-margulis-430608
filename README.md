A website to easily track and communicate with your congressional representatives
[data from](https://sunlightlabs.github.io/congress/index.html)


[Homepage](http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com:3000/)


1. Completed Rubric on time (5 points)
2. Website features (95 points)
	- Finding legislators page (30 points)
		- All legislators retrieved from api and listed (15 points)
		- List is sortable by name/state/chamber (15 points)
	- Legislator detail page (45 points)
		- View legislator details (name, party, term, etc.) (15 points)
		- View all sponsored bills (15 points)
		- Approve/disapprove of legislator and keep stats (15 points)
	- Creative Portion (20 points)

- See info for each bill (10 points)

- Approve/disapprove of bills and keep stats (10 points)